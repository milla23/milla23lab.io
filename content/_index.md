+++
title = "Proxecto de dirección"
+++
<!--: .wrap .size-70 .aligncenter bgimage=images/pencil.jpg -->


## **Presentación do proxecto**

Proxecto de dirección para o IES do Milladoiro | José L. Capón
{ .text-intro }

[{{< svg fa-book >}}Documento co proxecto (pdf)](proxecto_direccion_version_instituto.pdf)

---

<!--: .wrap .aligncenter bgimage=images/lienzo_ies_milladoiro.png -->

## **Introdución**

<blockquote> <p> Un proxecto de 4 cursos para o IES do Milladorio </p></blockquote>

---

<!--: .wrap .size-40 bgimage=images/lienzo_ies_milladoiro.png -->


## **Deseño**

- 4 principios educativos.
- 5 áreas de mellora.
- 12 obxectivos.
- 74 actuacións.
- 6 metas de logro xerais.

{.fadeInUp}

---

<!--: .wrap .size-40 bgimage=images/lienzo_ies_milladoiro.png -->

## **Principios educativos**

1. Participación integral.
2. Convivencia positiva e prácticas restaurativas.
3. Traballo cooperativo e distribuído.
4. Xestión axil dos proxectos.

---

<!--: .wrap bgimage=images/lienzo_ies_milladoiro.png -->

## **Obxectivos**. Ideas clave

|||

1. Arquitectura de aprendizaxe inclusiva.
2. Pensamento crítico.
3. Resolución pacífica de conflitos e cultura de paz.
4. Unha comunidade educativa vencellada.
5. Recursos e programas efectivos.
6. Orientación competencial.

|||

7. Colaboración coas institucións.
8. Modo de vida saudable.
9. Identidade dixital democrática.
10. Competencias docentes.
11. Procesos e procedementos de xestión do centro.
12. Excelencia.

---

<!--: .wrap bgimage=images/lienzo_ies_milladoiro.png -->

## **Avaliación**

- Primeira capa: atención aos compromisos do proxecto.
- Segundo capa: dirixida ao seguimento e mellora de proxectos.
- Terceira capa: valoración das finalidades do proxecto.
{.fadeInUp}
